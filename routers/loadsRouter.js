const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {
  createLoad,
  getLoads,
  getCurrentLoad,
  updateCurrentLoad,
  deleteCurrentLoad,
  postLoad,
  getActiveLoads,
  getLoadShippingInfo,
  changeState,
} = require('../controllers/loadsController');
const {loadValidate} = require('../middlewares/loadValidateMiddleware');

router.get('/',
    asyncWrapper(getLoads),
);

router.post('/',
    asyncWrapper(loadValidate),
    asyncWrapper(createLoad),
);

router.get('/active',
    asyncWrapper(getActiveLoads),
);

router.patch('/active/state',
    asyncWrapper(changeState),
);

router.get('/:id',
    asyncWrapper(getCurrentLoad),
);

router.put('/:id',
    asyncWrapper(updateCurrentLoad),
);

router.delete('/:id',
    asyncWrapper(deleteCurrentLoad),
);

router.post('/:id/post',
    asyncWrapper(postLoad),
);

router.get('/:id/shipping_info',
    asyncWrapper(getLoadShippingInfo),
);

module.exports = router;
