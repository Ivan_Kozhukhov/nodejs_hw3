const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {validatePassword} = require('../middlewares/passwordValidateMiddleware');
const {
  deleteUser,
  getUserInfo,
  updateUserPassword,
} = require('../controllers/usersController');

router.get(
    '/me',
    asyncWrapper(getUserInfo),
);

router.delete(
    '/me',
    asyncWrapper(deleteUser),
);

router.patch(
    '/me/password',
    asyncWrapper(validatePassword),
    asyncWrapper(updateUserPassword),
);

module.exports = router;
