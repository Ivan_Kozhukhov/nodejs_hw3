const ResponseError = require('../errorTypes/ResponseError');
const {Truck} = require('../models/truckModel');

const checkTruckExist = async (req, res) => {
  const truckId = req.params.id;
  const {_id: userId} = res.locals;

  const truck = await Truck.findOne({_id: truckId});

  if (truck.created_by !== userId) {
    throw new ResponseError('You do not have truck with this ID');
  }
};

const checkIfTruckAssigned = async (req, res) => {
  const truckId = req.params.id;
  const truck = await Truck.findOne({_id: truckId});

  if (truck.assigned_to !== null) {
    throw new ResponseError('You can\'t do something to assigned trucks');
  }
};

const checkIfUserAssignedTruck = async (req, res) => {
  const {_id} = res.locals;
  const allTrucks = await Truck.find({assigned_to: _id});

  if (allTrucks.length > 0) {
    throw new ResponseError('You can assign only one truck');
  }
};

const getAssignedTruck = async (id) => {
  if(id === null) {
    throw new ResponseError('This load is not assigned');
  }

  const truck = await Truck.findOne({assigned_to: id}, {__v: 0});

  return truck;
};


module.exports = {
  checkTruckExist,
  checkIfTruckAssigned,
  checkIfUserAssignedTruck,
  getAssignedTruck,
};
