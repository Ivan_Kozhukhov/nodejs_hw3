const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const saltRounds = bcrypt.genSaltSync(10);

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: `No user with email ${email} found!`,
    });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: 'Wrong password!'});
  }

  const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET);
  res.status(200).json({jwt_token: token});
};

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;
  const user = new User({
    email,
    password: await bcrypt.hashSync(password, saltRounds),
    role,
  });

  await user.save();

  res.json({message: 'User created successfully'});
};

