const {User} = require('../models/userModel');
const {isShipper} = require('../services/shipperService');
const bcrypt = require('bcryptjs');
const saltRounds = bcrypt.genSaltSync(10);

module.exports.deleteUser = async (req, res) => {
  await isShipper(req, res);
  const {_id} = res.locals;
  await User.findByIdAndDelete({_id});
  res.json({message: 'Profile deleted successfully'});
};

module.exports.getUserInfo = async (req, res) => {
  const {_id} = res.locals;

  const user = await User.findById(_id, {__v: 0, password: 0, role: 0});
  res.json({user});
};

module.exports.updateUserPassword = async (req, res) => {
  const {_id} = res.locals;
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(_id);

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'Wrong password!'});
  }

  await User.findOneAndUpdate(
      {_id},
      {password: await bcrypt.hashSync(newPassword, saltRounds)},
  );

  res.json({message: 'Password changed successfully'});
};
