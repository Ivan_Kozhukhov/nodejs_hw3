const Joi = require('joi');
module.exports.truckValidate = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .pattern(new RegExp(/^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT)$/i))
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
