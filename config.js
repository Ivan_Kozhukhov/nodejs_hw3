module.exports = {
  JWT_SECRET: process.env.JWT_SECRET,
  PORT: process.env.PORT,
  DB_USER: process.env.DB_USER || 'Ivan',
  DB_PASS: process.env.DB_PASS,
  DB_HOSTNAME: process.env.DB_HOSTNAME,
  LOAD_STATES: [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'
  ]
};
